import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
/* components  */
import GamesPage from 'components/pages/GamesPage';
import GameViewPage from 'components/pages/GameViewPage';
import LoginPage from './components/pages/LoginPage';

function App() {
	return (
		<Router>
			<Switch>
				<Route path='/login' component={LoginPage} />
				<Route path='' component={GamesPage} />
				<Route path='/game_view' component={GameViewPage} />
			</Switch>
			<ToastContainer autoClose={2000} />
		</Router>
	);
}

export default App;
