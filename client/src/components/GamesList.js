import React from 'react';
import GamesListItem from 'components/GamesListItem';
import { Table } from 'reactstrap';

const GamesList = props => {
	const { games, isLoaded, user } = props;
	return (
		<div>
			<h1>Games List</h1>
			<Table>
				<thead>
					<tr>
						<th>Game</th>
						<th>Creation Date</th>
						<th>Player</th>
						<th>Player</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{isLoaded &&
						games.map(game => {
							return <GamesListItem key={game.id} game={game} user={user} />;
						})}
				</tbody>
			</Table>
		</div>
	);
};

// const GamesListItem = ({ game: { id, created, gamePlayers }, user }) => (
// 	<tr>
// 		<td>{id}</td>
// 		<td>{moment().from(new Date(created), true) + ' ago'}</td>
// 		{/* Players */}
// 		{gamePlayers.length === 2 ? (
// 			gamePlayers.map(gp => <td key={gp.id}>{gp.name}</td>)
// 		) : (
// 			<>
// 				<td>{gamePlayers[0].name}</td>
// 				<td></td>
// 			</>
// 		)}
// 	</tr>
// );

export default GamesList;
