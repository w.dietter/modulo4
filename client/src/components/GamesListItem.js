import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';

const GamesListItem = ({ game, user }) => {
	const { id, created, gamePlayers } = game;

	if (gamePlayers.length < 2) {
		return (
			<tr>
				<td>{id}</td>
				<td>{moment().from(new Date(created), true)}</td>
        {
          gamePlayers[0].name === user
             ?  (<td><Link to={`/game_view?gp=${gamePlayers[0].gpid}`}>{gamePlayers[0].name}</Link></td>) 
        }
			</tr>
		);
	}
	return (
		<tr>
			<td>{id}</td>
		</tr>
	);
};

export default GamesListItem;

// const GamesListItem = ({ game: { id, created, gamePlayers }, user }) => (
// 	<tr>
// 		<td>{id}</td>
// 		<td>{moment().from(new Date(created), true) + ' ago'}</td>
// 		{/* Players */}
// 		{gamePlayers.length === 2 ? (
// 			gamePlayers.map(gp => <td key={gp.id}>{gp.name}</td>)
// 		) : (
// 			<>
// 				<td>{gamePlayers[0].name}</td>
// 				<td></td>
// 			</>
// 		)}
// 	</tr>
// );
