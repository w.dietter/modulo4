import React from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

const INITIAL_STATE = {
	username: '',
	password: ''
};
class LoginForm extends React.Component {
	state = {
		...INITIAL_STATE
	};

	handleChange = ({ target: { value, name } }) => {
		this.setState({
			[name]: value
		});
	};

	handleSubmit = async e => {
		e.preventDefault();
		console.log(this.state);
		try {
			const res = await axios.post(
				'/api/login',
				new URLSearchParams({ ...this.state })
			);
			if (res.status === 200) {
				toast.success('Logged in!');
			}
			console.log(res);
		} catch (err) {
			console.log(err);
		}
	};

	render() {
		const { username, password } = this.state;
		return (
			<Form onSubmit={this.handleSubmit}>
				<FormGroup>
					<Label for='username'>Username</Label>
					<Input
						type='text'
						name='username'
						value={username}
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<Label for='passwrod'>Password</Label>
					<Input
						type='password'
						name='password'
						value={password}
						onChange={this.handleChange}
					/>
				</FormGroup>
				<Button color='primary' type='submit'>
					Login
				</Button>
			</Form>
		);
	}
}

export default LoginForm;
