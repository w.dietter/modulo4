import React from 'react';
import axios from 'axios';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

const INITIAL_STATE = {
	username: '',
	password: ''
};

class RegisterForm extends React.Component {
	state = {
		...INITIAL_STATE
	};

	handleChange = ({ target: { value, name } }) => {
		this.setState({
			[name]: value
		});
	};

	handleSubmit = async e => {
		e.preventDefault();
		console.log(this.state);
		try {
			const res = await axios.post(
				'/api/players',
				new URLSearchParams({ ...this.state })
			);
			console.log(res);
		} catch (err) {
			console.log(err);
		}
	};

	render() {
		const { username, password } = this.state;
		return (
			<Form onSubmit={this.handleSubmit}>
				<FormGroup>
					<Label for='username'>Username</Label>
					<Input
						type='text'
						name='username'
						value={username}
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<Label for='passwrod'>Password</Label>
					<Input
						type='password'
						name='password'
						value={password}
						onChange={this.handleChange}
					/>
				</FormGroup>
				<Button color='info' type='submit'>
					Register
				</Button>
			</Form>
		);
	}
}

export default RegisterForm;
