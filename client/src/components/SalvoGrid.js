import React from 'react';
import { Container } from 'reactstrap';
import { Cell } from 'elements';
import SalvoGridCell from 'components/SalvoGridCell';

class SalvoGrid extends React.Component {
	static defaultProps = {
		rows: ['A', 'B', 'C', 'D', 'E', 'F'],
		cols: [1, 2, 3, 4, 5, 6]
	};

	async componentDidMount() {
		try {
		} catch (err) {
			console.log(err);
		}
	}

	render() {
		const { rows, cols } = this.props;
		return (
			<Container>
				<GridHeader cols={cols} />
				{rows.map(row => {
					return <GridRow key={row} row={row} cols={cols} />;
				})}
			</Container>
		);
	}
}

const GridHeader = props => (
	<div className='d-flex'>
		<Cell />
		{props.cols.map((col, i) => {
			return <Cell key={col}>{col}</Cell>;
		})}
	</div>
);

const GridRow = ({ row, cols }) => (
	<div className='d-flex'>
		<Cell header>{row}</Cell>
		{cols.map(col => {
			return <SalvoGridCell key={`${row + col}`} id={`${row + col}`} />;
		})}
	</div>
);

export default SalvoGrid;
