import React from 'react';
import axios from 'axios';
import SalvoGrid from 'components/SalvoGrid';

class GameViewPage extends React.Component {
	async componentDidMount() {
		const search = new URLSearchParams(this.props.location.search);
		const gamePlayer = search.get('gp');
		try {
			const res = await axios.get(`/api/game_view/${gamePlayer}`);
			console.log(res);
		} catch (err) {
			console.log(err);
		}
	}

	render() {
		return (
			<div>
				<SalvoGrid />
			</div>
		);
	}
}

export default GameViewPage;
