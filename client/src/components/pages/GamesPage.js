import React from 'react';
import axios from 'axios';
import {
	sortGamesByDate,
	getPlayersLeaderboardData
} from 'helpers/gamesHelpers';
/* Component */
import Leaderboard from 'components/Leaderboard';
import GamesList from 'components/GamesList';

class GamesPage extends React.Component {
	state = {
		games: [],
		user: '',
		players: [],
		isLoaded: false
	};
	async componentDidMount() {
		try {
			const res = await axios.get('/api/games');
			let { games, player } = res.data;
			games = sortGamesByDate(games);
			const players = getPlayersLeaderboardData(games);
			console.log(res);
			this.setState({
				games,
				user: player,
				players,
				isLoaded: true
			});
		} catch (err) {
			console.log(err);
			console.log(err.response);
		}
	}
	render() {
		const { players, isLoaded, user, games } = this.state;
		return (
			<div>
				<GamesList user={user} isLoaded={isLoaded} games={games} />
				<h2>Leaderboard</h2>
				<Leaderboard players={players} isLoaded={isLoaded} />
			</div>
		);
	}
}

export default GamesPage;
