import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import LoginForm from 'components/LoginForm';
import RegisterForm from 'components/RegisterForm';

class LoginPage extends React.Component {
	render() {
		return (
			<Container>
				<Row>
					<Col>
						<h1>Login</h1>
						<LoginForm />
					</Col>
					<Col>
						<h1>Register</h1>
						<RegisterForm />
					</Col>
				</Row>
			</Container>
		);
	}
}

export default LoginPage;
