import styled from 'styled-components';

export const Cell = styled.div`
	width: 4rem;
	height: 4rem;
	border: 1px dashed #444;
	background: ${props => (props.header ? 'blue' : 'dodgerblue')};
	display: flex;
	justify-content: center;
	align-items: center;
	color: white;
`;
