package com.codeoftheweb.salvo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
public class GamePlayer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Player player;

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private Set<Ship> ships = new HashSet<>();

    @OneToMany(mappedBy = "gamePlayer", fetch = FetchType.EAGER)
    private List<Salvo> salvos = new ArrayList<>();

    private Date joinDate;

    public GamePlayer() {
    }

    public GamePlayer(Game game, Player player, Date joinDate) {
        this.game = game;
        this.player = player;
        this.joinDate = joinDate;
    }

    public Map<String, Object> gamePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("player", this.getPlayer()
                              .playerDTO());
        return dto;
    }

    public Map<String, Object> gamesWithPlayersDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("gpid", this.getId());
        dto.put("id", this.getPlayer().getId());
        dto.put("name", this.getPlayer().getUserName());
        dto.put("score", this.getScore() != null ?
                this.getScore().scoreDTO() : null);
        return dto;
    }

    public Map<String, Object> simpleGamePlayerDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", this.getId());
        dto.put("player", this.getPlayer()
                              .playerDTO());
        dto.put("game", this.getGame()
                            .simpleGameDTO());
        dto.put("score", this.getScore() != null ? this.getScore()
                                                       .scoreDTO() : null);
        return dto;
    }

    public Score getScore() {
        Score score = this.getPlayer()
                          .getScore(this.getGame());
        if (score != null) {
            return score;
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }

    public List<Salvo> getSalvos() {
        return salvos;
    }

}
