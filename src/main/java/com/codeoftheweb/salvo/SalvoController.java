package com.codeoftheweb.salvo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@RestController
@RequestMapping(path = "/api")
public class SalvoController {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GamePlayerRepository gamePlayerRepository;

    @Autowired
    private SalvoRepository salvoRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(path = "/games/players/{gamePlayerId}/ships", method =
            RequestMethod.POST)
    public ResponseEntity<Object> placeShips(@RequestBody Set<Ship> ships,
                                             @PathVariable Long gamePlayerId,
                                             Authentication auth) {
        Player player = getPlayerOfAuthentication(auth);
        if(player == null) {
            return new ResponseEntity<>(simpleJson("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }
        GamePlayer gp =
                gamePlayerRepository.findById(gamePlayerId).orElse(null);
        if(gp == null || gp.getPlayer().getUserName() != player.getUserName()) {
            return new ResponseEntity<>(simpleJson("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }
        Set<Ship> alreadyLocatedShips = gp.getShips();
        if(alreadyLocatedShips.size() > 0) {
            return new ResponseEntity<>(simpleJson("msg", "Ships already " +
                    "placed"), HttpStatus.FORBIDDEN);
        }

        for(Ship ship: ships) {
            ship.setGamePlayer(gp);
            shipRepository.save(ship);
        }
//        gp.setShips(ships);
//        gamePlayerRepository.save(gp);
      return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createGame(
            Authentication authentication) {

        Player player = getPlayerOfAuthentication(authentication);
        if (player == null) {
            return new ResponseEntity<>(simpleJson("msg", "unauthorized"),
                    HttpStatus.UNAUTHORIZED);
        }

        Game game = new Game(new Date());
        gameRepository.save(game);
        GamePlayer gp = new GamePlayer(game, player, new Date());
        Long gpid = gamePlayerRepository.save(gp)
                                        .getId();
        return new ResponseEntity<>(simpleJson("gpid", gpid),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/game/{gameId}/players", method =
            RequestMethod.POST)
    public ResponseEntity<Object> joinGame(
            @PathVariable Long gameId, Authentication auth) {
        Player player = getPlayerOfAuthentication(auth);
        if (player == null) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Game game = gameRepository.findById(gameId)
                                  .orElse(null);
        if (game == null) {
            return new ResponseEntity<>(simpleJson("msg", "No such game"),
                    HttpStatus.FORBIDDEN);
        }
        Set<GamePlayer> gamePlayers = game.getGamePlayers();
        if (gamePlayers.size() > 1) {
            return new ResponseEntity<>(simpleJson("msg", "Game is full"),
                    HttpStatus.FORBIDDEN);
        }
        GamePlayer gp = new GamePlayer(game, player, new Date());
        Long gpid = gamePlayerRepository.save(gp)
                                        .getId();
        return new ResponseEntity<>(simpleJson("gpid", gpid),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> createNewPlayer(
            @RequestParam String username,
            @RequestParam String password) {

        if (username.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>(simpleJson("msg", "Missing Data"),
                    HttpStatus.FORBIDDEN);
        }

        if (playerRepository.findByUserName(username) != null) {

            return new ResponseEntity<>(simpleJson("msg", "Name in use"),
                    HttpStatus.CONFLICT);
        }
        Player newPlayer = new Player(username,
                passwordEncoder.encode(password));
        playerRepository.save(newPlayer);
        return new ResponseEntity<>(simpleJson("player",
                newPlayer.getUserName()),
                HttpStatus.CREATED);
    }

    @RequestMapping(path = "/game_view/{gamePlayerId}")
    public ResponseEntity<Map<String, Object>> getGameViewByGamePlayerId(
            @PathVariable Long gamePlayerId, Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository
                .findById(gamePlayerId)
                .orElse(null);
        if (gamePlayer != null && (gamePlayer.getPlayer()
                                             .getUserName() ==
                authentication.getName())) {
            return new ResponseEntity<>(gameViewDTO(gamePlayer),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(simpleJson("msg",
                    "unauthorized"), HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/games")
    public Map<String, Object> getGames(Authentication authentication) {
        Map<String, Object> gamesDTO = new LinkedHashMap<>();
        Set<Game> gameSet = new HashSet<>(gameRepository.findAll());
        Player player = getPlayerOfAuthentication(authentication);
        gamesDTO.put("player", player != null ? player.playerDTO() : null);
        gamesDTO.put("games", gameSet
                .stream()
                .map(g -> g.gameDTO())
                .collect(toList()));
        return gamesDTO;
    }

    private Map<String, Object> simpleJson(String key, Object value) {
        Map<String, Object> simpleJson = new LinkedHashMap<>();
        simpleJson.put(key, value);
        return simpleJson;
    }

    private Player getPlayerOfAuthentication(Authentication authentication) {
        if (authentication != null && authentication.getName() != null) {
            return playerRepository.findByUserName(authentication.getName());
        }
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return null;
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }

    private List<? extends Object> salvoFlatListDTO(
            Set<GamePlayer> gamePlayers) {
        List<Map<String, Object>> flatListSalvos = new ArrayList<>();
        for (GamePlayer gp : gamePlayers) {
            flatListSalvos.addAll(gp.getSalvos()
                                    .stream()
                                    .map(s -> s.salvoFlatListDTO())
                                    .collect(toSet()));
        }
        return flatListSalvos;
    }

    private Map<String, Object> salvoByTurnDTO(Set<GamePlayer> gamePlayers) {
        Map<String, Object> dto = new LinkedHashMap<>();
        for (GamePlayer gp : gamePlayers) {
            List<Salvo> salvos = gp.getSalvos();
            dto.put(String.valueOf(gp.getPlayer()
                                     .getId()),
                    setOfTurnAndLocationsDTO(salvos));
        }
        return dto;
    }

    public static Set<Map<String, Object>> setOfTurnAndLocationsDTO(
            List<Salvo> salvos) {
        return salvos.stream()
                     .map(s -> s.salvoTurnAndLocationsDTO())
                     .collect(Collectors.toSet());
    }

    private Map<String, Object> gameViewDTO(GamePlayer gamePlayer) {
        Map<String, Object> gameViewDTO = new LinkedHashMap<String, Object>();
        Set<GamePlayer> gamePlayersOfThisGame = gamePlayer.getGame()
                                                          .getGamePlayers();
        List<Map<String, Object>> ships = gamePlayer
                .getShips()
                .stream()
                .map(s -> s.getShipDTO())
                .collect(toList());

        gameViewDTO.put("id",
                gamePlayer.getGame()
                          .getId());
        gameViewDTO.put("created",
                gamePlayer.getGame()
                          .getCreationDate()
                          .getTime());
        gameViewDTO.put("gamePlayers",
                gamePlayer.getGame()
                          .getGamePlayers()
                          .stream()
                          .map(gp -> gp.gamePlayerDTO())
                          .collect(toList()));
        gameViewDTO.put("ships", ships);
        gameViewDTO.put("salvosFlatList",
                salvoFlatListDTO(gamePlayersOfThisGame));
        gameViewDTO.put("salvosByPlayer",
                salvoByTurnDTO(gamePlayersOfThisGame));
        return gameViewDTO;
    }
}
