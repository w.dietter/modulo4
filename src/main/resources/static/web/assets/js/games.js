/* Function definitios */

const getGames = async () => {
	/* fetch data from backend */
	try {
		const response = await fetch('http://localhost:8080/api/games');
		const gamesData = await response.json();
		console.log(gamesData);
		const { games = [], player } = gamesData || {};
		/* 
		player_
		email
		id*/
		renderAuthForms(player);
		/* sort games by creation date */
		games.sort((a, b) => {
			return a.created < b.created ? -1 : 1;
		});
		/* render games list */
		createGameListHTML(games, player);
		/* create leaderboard logic -> mutates games array! */
		const players = getPlayersLeaderboard(games);
		/* sort player order */
		players.sort((a, b) => {
			if (a.score > b.score) return -1;
			else if (a.score < b.score) return 1;
			else if (a.score === b.score) return 0;
		});
		players.sort((a, b) => {
			if (a.losses < b.losses) return -1;
			else if (a.losses > b.losses) return 1;
			else if (a.losses === b.losses) return 0;
		});

		/* render leaderboard */
		createLeaderboardHTML(players);
	} catch (error) {
		console.log(error);
	}
};

const getPlayersLeaderboard = games => {
	/* games: [ id:string, created: number, gamePlayers: [{id, gpid, name, score: {score}}]] */
	let players = [];
	games.forEach(game => {
		game.gamePlayers.forEach(({ id, name, score: gpScore }) => {
			const { score } = gpScore || {};
			let pi = players.findIndex(p => p.id === id);
			if (pi !== -1) {
				if (score >= 0) {
					players[pi].wins =
						score == 1 ? players[pi].wins + 1 : players[pi].wins;
					players[pi].ties =
						score == 0.5 ? players[pi].ties + 1 : players[pi].ties;
					players[pi].losses =
						score == 0 ? players[pi].losses + 1 : players[pi].losses;
					players[pi].score = score
						? players[pi].score + score
						: players[pi].score;
				}
			} else {
				let playerData = {
					id,
					name,
					wins: score == 1 ? 1 : 0,
					ties: score == 0.5 ? 1 : 0,
					losses: score == 0 ? 1 : 0,
					score: score || 0
				};
				players.push(playerData);
			}
		});
	});
	console.log(players);
	return players;
};

const renderAuthForms = player => {
	/* showing and hiding login and logout */
	if (player) {
		document.querySelector(
			'#currentPlayer'
		).innerText = `Current Player: ${player.email}`;
		document.querySelector('#loginForm').classList.add('hide');
		document.querySelector('#signinForm').classList.add('hide');
		document.querySelector('#createGameBtn').classList.remove('hide');
	} else {
		document.querySelector('#logoutBtn').classList.add('hide');
		document.querySelector('#createGameBtn').classList.add('hide');
	}
};

const createLeaderboardHTML = (players = []) => {
	const leaderboardTBody = document.querySelector('#leaderboard-tbody');
	players.forEach(player => {
		if (player.wins > 0 || player.losses > 0 || player.ties > 0) {
			let tr = document.createElement('tr');

			let nameTd = document.createElement('td');
			nameTd.innerText = player.name;

			let totalScoreTd = document.createElement('td');
			totalScoreTd.innerText = player.score;

			let winsTd = document.createElement('td');
			winsTd.innerText = player.wins;

			let lossesTd = document.createElement('td');
			lossesTd.innerText = player.losses;

			let tiesTd = document.createElement('td');
			tiesTd.innerText = player.ties;

			tr.appendChild(nameTd);
			tr.appendChild(totalScoreTd);
			tr.appendChild(winsTd);
			tr.appendChild(lossesTd);
			tr.appendChild(tiesTd);
			leaderboardTBody.appendChild(tr);
		}
	});
};

const createGameListHTML = (games = [], currentUserName = {}) => {
	const gameListTBody = document.querySelector('#gameList-tbody');
	games.forEach(({ gamePlayers, id, created }) => {
		let gameTr = document.createElement('tr');
		/* tds */
		gameIdTd = document.createElement('td');
		gameIdTd.innerText = id;

		creationDateTd = document.createElement('td');
		creationDateTd.innerText = new Date(created).toLocaleString();

		gameTr.appendChild(gameIdTd);
		gameTr.appendChild(creationDateTd);

		gamePlayers.forEach(player => {
			if (currentUserName && player.name === currentUserName.email) {
				const gameViewLink = document.createElement('a');
				gameViewLink.setAttribute('href', `/web/game.html?gp=${player.gpid}`);
				gameViewLink.innerText = player.name;

				const playerTd = document.createElement('td');
				playerTd.setAttribute('data-gpid', player.gpid);
				playerTd.appendChild(gameViewLink);

				gameTr.appendChild(playerTd);
			} else {
				const playerTd = document.createElement('td');
				playerTd.setAttribute('data-gpid', player.gpid);
				playerTd.innerText = player.name;

				gameTr.appendChild(playerTd);
			}
		});
		/* if game has one player create Join Game button */
		if (currentUserName && gamePlayers.length === 1) {
			const joinBtnTd = document.createElement('td');
			const joinBtn = document.createElement('button');
			joinBtn.setAttribute('data-gameId', id);
			joinBtn.innerText = 'JOIN GAME';
			/* creates eventListener for join game button and bind the id value of each game */
			joinBtn.addEventListener('click', handleJoinGame.bind(this, id));

			joinBtnTd.appendChild(joinBtn);
			gameTr.appendChild(document.createElement('td'));
			gameTr.appendChild(joinBtnTd);
		}
		gameListTBody.appendChild(gameTr);
	});
};

const handleJoinGame = async gameId => {
	try {
		const response = await fetch(
			`http://localhost:8080/api/game/${gameId}/players`,
			{
				method: 'POST'
			}
		);
		console.log(response);
		if (response.status === 201) {
			window.location.replace(`/web/game.html?gp=${gameId}`);
		}
		if (response.status === 403) {
			const data = await response.json();
			alert(data.msg);
		}
	} catch (error) {
		console.log(error);
	}
};

const loginHandler = async e => {
	e.preventDefault();
	const username = document.querySelector('#login-username');
	const password = document.querySelector('#login-password');
	try {
		const responseData = await login(username.value, password.value);
		console.log(responseData);
		username.value = '';
		password.value = '';
	} catch (error) {
		alert(error.message);
	}
};

const logoutHandler = async e => {
	const reload = true;
	await logout(reload);
};

const signinHandler = async e => {
	e.preventDefault();

	const username = document.querySelector('#signin-username');
	const password = document.querySelector('#signin-password');

	if (username.length === 0 || password.length === 0) {
		return alert('Username and password are required');
	}

	try {
		const response = await fetch('http://localhost:8080/api/players', {
			method: 'POST',
			headers: {
				'Content-type': 'application/x-www-form-urlencoded'
			},
			body: new URLSearchParams({
				username: username.value,
				password: password.value
			})
		});

		if (response.status === 201) {
			alert('Player created!');
			await login(username.value, password.value);
		} else {
			username.value = '';
			password.value = '';
			const data = await response.json();
			console.log(data.msg || 'error');
		}
	} catch (error) {
		console.log(error);
	}
};

const createGameHandler = async () => {
	try {
		const response = await fetch('http://localhost:8080/api/games', {
			method: 'POST'
		});

		if (response.status === 201) {
			const data = await response.json();
			window.location.replace(`game.html?gp=${data.gpid}`);
		} else {
			alert('Error while creating new game.');
		}
	} catch (error) {
		console.log(error);
	}
};

/* ***********************************************************  */

/* selectors*/
const loginBtn = document.querySelector('#loginBtn');
const logoutBtn = document.querySelector('#logoutBtn');
const signinBtn = document.querySelector('#signinBtn');
const createGameBtn = document.querySelector('#createGameBtn');

/* event listeners */
loginBtn.addEventListener('click', loginHandler);
logoutBtn.addEventListener('click', logoutHandler);
signinBtn.addEventListener('click', signinHandler);
createGameBtn.addEventListener('click', createGameHandler);

/* main */
getGames();
